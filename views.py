from django.shortcuts import render
# Create your views here.
def profile(request):
 # response = {'name' : mhs_name}
 return render(request,'Profile.html')

def experience(request):
	return render(request, 'Experience.html')

def form(request):
	return render(request, 'Form.html')

def story(request):
	return render(request, 'Story 3.html')