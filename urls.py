from django.conf.urls import url

from .views import profile
from .views import story
from .views import experience
from .views import form
urlpatterns = [
    url(r'^Profile', profile, name='profile'),
     url(r'^Experience', experience, name='experience'),
      url(r'^Form', form, name='form'),
       url(r'^Story', story, name='story')]